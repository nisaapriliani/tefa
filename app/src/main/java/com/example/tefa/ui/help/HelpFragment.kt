package com.example.tefa.ui.help

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.tefa.R
import com.example.tefa.base.BaseFragment
import com.example.tefa.databinding.FragmentHelpBinding
import com.example.tefa.databinding.FragmentHomeBinding
import com.kodekita.toaster.ToasterMessage

class HelpFragment : BaseFragment() {
    private lateinit var binding: FragmentHelpBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentHelpBinding.inflate(inflater, container, false)
        initAction()
        return binding.root
    }

    private fun initAction() {
        with(binding) {
            tvCall.setOnClickListener {
                try {
                    val dialIntent = Intent(Intent.ACTION_DIAL)
                    dialIntent.data = Uri.parse("tel:" + "112")
                    startActivity(dialIntent)
                } catch (e: Exception) {
                    ToasterMessage.toaster(requireContext(), e.message.toString())
                    Log.e("Error", e.message.toString())
                }
            }
        }
    }

}