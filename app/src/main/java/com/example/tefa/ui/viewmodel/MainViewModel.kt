package com.example.tefa.ui.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.tefa.data.model.UserCreateResponse
import com.example.tefa.data.model.UserItem
import com.example.tefa.data.model.request.UserRequest
import com.example.tefa.data.remote.Repository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(private val repository: Repository) : ViewModel() {

    private val _isLoading = MutableLiveData<Boolean>()
    val isLoading: LiveData<Boolean> = _isLoading

    private val _userList = MutableLiveData<List<UserItem>>()
    val userList: LiveData<List<UserItem>> = _userList

    val userCreated = MutableLiveData<UserCreateResponse>()

    fun getUserList() {
        viewModelScope.launch {
            try {
                _isLoading.postValue(true)
                val response = repository.getUserList()
                _userList.postValue(response)
            } catch (e: Exception) {
                e.printStackTrace()
            } finally {
                _isLoading.postValue(false)
            }
        }
    }
    fun createUsers(UserRequest: UserRequest) {
        viewModelScope.launch {
            try {
                _isLoading.postValue(true)
                val response = repository.createUser(UserRequest)
                userCreated.postValue(response)
            } catch (e: Exception) {
                e.printStackTrace()
            } finally {
                _isLoading.postValue(false)
            }
        }
    }

}