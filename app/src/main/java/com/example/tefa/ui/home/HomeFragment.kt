package com.example.tefa.ui.home

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.core.content.ContextCompat
import com.example.tefa.R
import com.example.tefa.base.BaseFragment
import com.example.tefa.databinding.FragmentHomeBinding
import com.example.tefa.ui.MainActivity
import com.example.tefa.ui.home.adapter.BannerSliderAdapter
import com.example.tefa.ui.student.ListStudentActivity
import com.example.tefa.utils.IntentKey
import com.example.tefa.utils.autoScroll
import com.example.tefa.utils.load
import com.example.tefa.utils.makeBannerIndicator
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

class HomeFragment : BaseFragment() {

    private lateinit var binding: FragmentHomeBinding
    private val listItem = mutableListOf<Int>()
    private val listDots = mutableListOf<ImageView>()
    private var avatarUrl = ""
    private var uriFile: Uri? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentHomeBinding.inflate(inflater, container, false)
        initIntent()
        initView()
        initAction()

        return binding.root
    }

    private fun initIntent() {
        avatarUrl = activity?.intent?.extras?.getString(IntentKey.KEY_USER) ?: ""
    }

    private fun initView() {
        listItem.add(R.drawable.banner_asset_1)
        listItem.add(R.drawable.banner_asset_2)
        listItem.add(R.drawable.banner_asset_3)

        val vpAdapter = BannerSliderAdapter(listItem)

        val formatter = DateTimeFormatter.ofPattern("dd MMMM YYYY")
        val currentDate = LocalDateTime.now().format(formatter)

        with(binding){
            ivAvatar.load(avatarUrl)
            vpBanner.adapter = vpAdapter
            llBannerIndicator.makeBannerIndicator(vpAdapter, listDots, requireContext())
            vpBanner.autoScroll(2000, listDots, requireContext())
            tvDateCheckin.text = currentDate
        }
    }

    private fun initAction() {
        with(binding){
            btnCheckIn.setOnClickListener {
                val formatter = DateTimeFormatter.ofPattern("HH.mm")
                val currentTime = LocalDateTime.now().format(formatter)
                tvTimeCheckIn.text = "$currentTime - 17.00"
            }

            ivAvatar.setOnClickListener {
                imagePickerGallery()
            }

            tvDetailsStudents.setOnClickListener {
                val intent = Intent(requireContext(), ListStudentActivity::class.java)
                startActivity(intent)
            }
        }
    }

    private fun imagePickerGallery() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(
                    requireActivity(),
                    Manifest.permission.READ_EXTERNAL_STORAGE
                ) == PackageManager.PERMISSION_DENIED
            ) {
                val permissions = arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE)
                requestPermissions(permissions, PERMISSION_CODE)
            } else {
                chooseImageGallery()
            }
        } else {
            chooseImageGallery()
        }
    }

    private fun chooseImageGallery() {
        val intent = Intent(Intent.ACTION_PICK)
        intent.type = "image/*"
        startActivityForResult(intent, IMAGE_CHOOSE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == IMAGE_CHOOSE && resultCode == Activity.RESULT_OK) {
            if (data != null) {
                uriFile = data.data
                binding.ivAvatar.setImageURI(data.data)
            }
        }
    }

    companion object {
        private const val PERMISSION_CODE = 1001
        private const val IMAGE_CHOOSE = 1000
    }
}