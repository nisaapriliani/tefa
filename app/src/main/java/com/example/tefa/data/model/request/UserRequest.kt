package com.example.tefa.data.model.request

data class UserRequest (
    val name: String,
    val job: String
        )