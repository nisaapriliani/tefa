package com.example.tefa.data.remote

import com.example.tefa.data.model.UserCreateResponse
import com.example.tefa.data.model.UserItem
import com.example.tefa.data.model.request.UserRequest
import com.example.tefa.data.remote.response.UserList
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiService {
    @GET("api/users")
    suspend fun getUserList(
        @Query("page") page: Int,
    ): UserList.Response

    @POST("api/users")
    suspend fun createUser(
        @Body bodyRequest: UserRequest,
    ): UserCreateResponse
}